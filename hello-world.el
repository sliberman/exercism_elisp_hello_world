;;; hello-world.el --- Hello World Exercise (exercism)

;;; Commentary:

;;; Code:

(defun hello ()
  "Hello world function"
  "Hello, World!")

(provide 'hello-world)
;;; hello-world.el ends here
